//Eli Frye
// 9/4/15
// comp 322
// This file is the driver for testing the HashTable template.
const long ADD = 133448349;
const long MULT = 23934;
#include "HashTable.h"
#include <string>
#include <iostream>
#include <cmath>
using std::string;
using std::hash;
using std::cout;
using std::endl;
class intHashFunctor
{
public:
	long operator () (long intToHash);
};
long intHashFunctor::operator()(long intToHash)
{
	return (intToHash * MULT) + ADD;
}
class floatHashFunctor
{
public:
	long operator () (float floatToHash);
};
long floatHashFunctor::operator()(float floatToHash)
{
	return (long) floor((floatToHash * MULT) + ADD);
}
int main()
{ 
	//ints:
	cout << "ints:" << endl;
	intHashFunctor ihf;
	HashTable<int,int, intHashFunctor> intHT(ihf,12,.2f);
	intHT.grow(14);
	intHT.insert(8, 27);
	intHT.insert(14, 238);
	intHT.insert(12,40);
	intHT.insert(27,40);
	intHT.insert(21,158);
	cout << intHT.search(21) << endl;
	cout << intHT.search(8) << endl;
	cout << intHT.search(14) << endl;
	cout << intHT.search(12) << endl;
	intHT.grow(26);
	cout << intHT.search(27) << endl;
	//floats:
	cout << "floats:" << endl;
	floatHashFunctor fhf;
	HashTable<int,float, floatHashFunctor> floatHT(fhf,9,.1f);
	floatHT.insert(8.4, 12);
	floatHT.insert(32.5, 23);
	floatHT.insert(238.55,23);
	floatHT.insert(23.5,40);
	floatHT.insert(35.2,134);
	cout << floatHT.search(23.5) << endl;
	cout << floatHT.search(8.4) << endl;
	cout << floatHT.search(32.5) << endl;
	cout << floatHT.search(238.55) << endl;
	cout << floatHT.search(35.2) << endl;
	//strings:
	cout << "strings" << endl;
	hash<string> h;
	HashTable<int, string, hash<string>> ht(h,15,0.3f);
	ht.grow(50);
	ht.insert("hello", 5);
	ht.insert("hi", 18);
	ht.insert("random",40);
	ht.insert("something",40);
	ht.insert("whatever",158);
	cout << ht.search("hello") << endl;
	cout << ht.search("hi") << endl;
	cout << ht.search("random") << endl;
	cout << ht.search("something") << endl;
	return 0;
}