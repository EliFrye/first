//Eli Frye
// 9/3/2015
// comp 322
// This file implements a hashtable template that uses chaining.
#include <vector>
#include <cmath>
#include <iostream>
using std::vector;
template<typename DataType, typename KeyType, typename Functor>
class HashTable
{
public:
	HashTable(Functor HashTableFunctor, int initialSize, float growthTrigger);
	~HashTable();
	void insert(const KeyType key, const DataType value);
	DataType search(KeyType key);//Returns the value that cooresponds to the key searched for.
	void grow(int newSize);
private:
	struct entry
	{
		KeyType key;
		DataType data;
	};
	typedef vector<entry> chain;
	chain* arr;
	int size;// Current size of array.
	double growthTrig;
	int numElements;//Number of elements in array
	Functor hashFunc;
	int determineNewSize();//Will return smallest prime number greater than double the current size.
	void insertWithoutGrowing(const KeyType key, const DataType value);
	bool isPrime(int number);
};
template<typename DataType, typename KeyType, typename Functor>
HashTable<typename DataType, typename KeyType, typename Functor>::HashTable(Functor HashTableFunctor, int initialSize, float growthTrigger)
{
	size =  initialSize;
	hashFunc = HashTableFunctor;
	growthTrig = growthTrigger;
	arr = new chain[initialSize];
	numElements = 0;
}
template<typename DataType, typename KeyType, typename Functor>
HashTable<typename DataType, typename KeyType, typename Functor>::~HashTable()
{
	delete [] arr;
}
template<typename DataType, typename KeyType, typename Functor>
void HashTable<typename DataType, typename KeyType, typename Functor>::insert(KeyType key, DataType value)
{
	numElements++;
	if((growthTrig*size)<numElements)
		grow(determineNewSize());
	entry newEntry = {key, value};
	(arr[hashFunc(key)%size]).push_back(newEntry);
	
}
template<typename DataType, typename KeyType, typename Functor>
void HashTable<typename DataType, typename KeyType, typename Functor>::grow(int newSize)
{
	int oldSize = size;
	size = newSize;
	chain* oldArr = arr;
	arr = new chain [newSize];
	for(int i = 0; i < oldSize; i++)
	{
		for(int j = 0; j < (oldArr[i]).size(); j++)
			insertWithoutGrowing(((oldArr[i]).at(j)).key,((oldArr[i]).at(j)).data);
	}
}
template<typename DataType, typename KeyType, typename Functor>
int HashTable<typename DataType, typename KeyType, typename Functor>::determineNewSize()//We multiply the size of the array by two and add one. Then we find increment the size of the hash table until its size is a prime number.
{
	int prospectiveNewSize = 2 * size + 1;
	while(true)//This loop continually tries new sizes for the array. If the size is a prime number, it is returned, otherwise we increment and try the new number.
	{
		if(isPrime(prospectiveNewSize))
			return prospectiveNewSize;
		prospectiveNewSize++;//The last number was not prime, so we try the next number as a size.
	}
	return 0;
}
template<typename DataType, typename KeyType, typename Functor>
DataType HashTable<typename DataType, typename KeyType, typename Functor>::search(KeyType key)
{
	try{
		chain bucketToSearch = arr[hashFunc(key) % size];
		for(int i = 0; i < bucketToSearch.size(); i++)
		{
			if((bucketToSearch.at(i)).key==key)
				return (bucketToSearch.at(i)).data;
		}
		throw 1;
	}
	catch(int)//If the key wasn't found, exit the program.
	{
		exit(1);
	}
}
template<typename DataType, typename KeyType, typename Functor>
void HashTable<typename DataType, typename KeyType, typename Functor>::insertWithoutGrowing(KeyType key, DataType value)
{
	entry newEntry = {key, value};//What we are going to insert in the hash table.
	(arr[hashFunc(key)%size]).push_back(newEntry);//Insert it.
}
template<typename DataType, typename KeyType, typename Functor>
bool HashTable<typename DataType, typename KeyType, typename Functor>::isPrime(int number)
{
	int testFactor = 2;//The first value we try as a factor to determine whether number is prime is 2.
	bool factorFound = false;
	while(!factorFound)//While a factor is not found.
		{
			if(number % testFactor)//If number is not divisible by test factor
				testFactor++;
			else
				factorFound=true;//Not prime, try the next number.
			if(testFactor > sqrt(number))//Once we reach the square root of number, we no longer need to try values as prime factors.
				return true;
		}
		return false;
}